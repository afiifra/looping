<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Looping</h2>
        <?php
        echo "<h3>Contoh 1</h3>";
        echo "<h4>Looping 1</h4>";
        for($i=1; $i<=19; $i+=2){
            echo $i. " - Angka Ganjil <br>";
        }
        echo "<h4>Looping 2</h4>";
        for($a=19; $a>=1; $a-=2){
            echo $a. " - Angka Ganjil <br>";
        }
        echo "<h3>Contoh 2</h3>";
        $nomor = [18,45,29,61,47,34];
        echo "Array Nomor : ";
        print_r($nomor);
        echo "<br>";
        foreach($nomor as $no){
            $rest[]= $no %=6;
        }

        echo "Array nomor sisa hasil bagi 6";
        print_r($rest);

        echo "<h3>Contoh 3</h3>";
        $data = [
            ["001","Keyboard logitech"," Keyboard yang mantap untuk kantoran","logitek.jpeg "],
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        
        ];
            
        foreach($data as $key => $value){
            $item = array(
                "id" => $value[0],
                "name" => $value[1],
                "price" => $value[2],
                "description" => $value[3],
                "source" => $value[4]
            );
            print_r($item);
            echo "<br>";
        }

        echo "<h3> Contoh 4</h3>";
        for($j=1; $j<=5; $j++){
            for($k=1; $k<=$j; $k++){
                echo "*";
            }
            echo "<br>";
        }


        ?>

  
</body>
</html> 
